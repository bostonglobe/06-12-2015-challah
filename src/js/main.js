(function() {
	'use strict';
	// global variables
	var $window = $(window);
	var $htmlBody = $('html, body');
	var $body = $('body');
	var $introImg = $('.intro--img');
	var $introBegin = $('.btn--audio');
	var $videoItem = $('.video-item');

	var _mobile = false;
	var _ipad = false;
	var _imgPath = 'assets/img/';
	var _dimensions = {w:0, h: 0};

	// graphic functions
	var init = function() {
		if(window.console && console.log) console.log('-- init globe graphic --');
		onResize();		
		setupMobile();
		setupEvents();
		Video.setup();
	};

	var setupMobile = function() {
		// go with full video experience or mobile fallbacks
		_mobile = isMobile.any();
		_ipad = isMobile.iPad();
		if(_mobile) {
			$body.addClass('mobile');
		}
		if(_ipad) {
			$body.addClass('ipad');	
		}
	};

	var setupEvents = function() {
		$window.on('resize', onResize);
		$introBegin.on('click', onBegin);
	};

	var onResize = function() {
		_dimensions.w = $window.width();
		_dimensions.h = $window.height();
		$introImg.css('height', _dimensions.h * 0.65);
		Waypoint.refreshAll();
	};

	var onBegin = function() {
		var player = Video.getPlayer('.video-part-1');
		if(!_mobile) {
			Video.playFromStart(player);
			Video.toggleMute(player);
		} else {
			Video.loadMobile(player);
		}
		$htmlBody.animate({
			scrollTop: $('.story').offset().top
		});
	};

	var Video = {
		path: 'assets/video/',
		players: {},

		setup: function() {
			var advance = function(index) {
				index++;
				if(index < $videoItem.length) {
					loadVideo(index);
				}
			};

			var loadVideo = function(index) {
				var el = $videoItem[index];
				// load video
				Video.insert(el, function() {
					if(!_mobile) {
						var player = Video.getPlayer(el);
						Video.setupWaypoint(player);
					}
					// give it a little buffer love
					setTimeout(function() {
						advance(index);
					}, 1000);
				});
			};

			if($videoItem.length) {
				loadVideo(0);
			}
		},

		insert: function(selector, cb) {
			var el = $(selector);
			var id = el.attr('data-src');

			Video.players[id] = {
				id: id,
				selector: selector,
				el: el.find('.video-item--element'),
				progressEl: el.find('.video-item--progress'),
				muteEl: el.find('.video-item--mute'),
				loaded: false,
				inView: false,
				playing: false,
				restarted: _ipad
			};

			var player = Video.players[id];

			// add a play button since cannot auto play
			if(_ipad || _mobile) {
				el.append('<div class="video-item--play"><img src="https://apps.bostonglobe.com/common/img/play.svg"/></div>');
				player.ipadEl = player.el.siblings('.video-item--play');
				var inject = document.querySelectorAll('.video-item--play img');
    			SVGInjector(inject);
			}

			player.el.jPlayer({
				swfPath: 'js/lib',
				supplied: 'm4v',
				loop: true,
				volume: 0,
				preload: 'auto',
				size: {width: '100%', height: 'auto' },
				
				ready: function() {
					player.el.jPlayer('setMedia', {
						poster: _imgPath + id + '.jpg',
						m4v: Video.path + id + '.mp4'
					});
					if(_mobile) {
						Video.setupClickEvent(player);
						cb();
					}
				},

				error: function(e) { console.log(player.id, 'video error'); },
				abort: function(e) { console.log(player.id, 'video abort'); },

				play: function(e) {
					player.playing = true;
					if(_ipad) {
						player.ipadEl.addClass('hide');
					}
				},
				pause: function(e) {
					player.playing = false;
					if(_ipad) {
						player.ipadEl.removeClass('hide');
					}
				},
				ended: function(e) {
					// player.playing = false;
					// player.el.jPlayer('playHead', 0);
				},
				canplaythrough: function() {
					//only fire once, not on loop
					if(!player.loaded) {
						player.loaded = true;
						if(!_mobile) {
							Video.setupClickEvent(player);
							cb();	
						}
					}
				},
				timeupdate: function(e) {
					if(player.progressEl) {
						var duration = e.jPlayer.status.duration;
            			var position = e.jPlayer.status.currentTime;
            			if(duration > 0) {
            				var percent = position / duration * 100 + '%';
            				player.progressEl.css('width', percent);
            			}
					}
				}
			});
		},

		toggle: function(player) {
			if(player && player.loaded) {
				// play or pause
				if(player.playing) {
					if(player.restarted) {
						Video.pause(player);
					} else {
						Video.toggleMute(player);
					}
					
				} else {
					Video.play(player);
				}
			}
		},

		getPlayer: function(selector) {
			var src = $(selector).attr('data-src');
			return Video.players[src];
		},

		play: function(player) {
			if(player && player.loaded) {
				if(!player.playing) {
					player.el.jPlayer('play');
				}
			}
		},

		playFromStart: function(player) {
			player.restarted = true;
			if(player && player.loaded) {
				if(!player.playing) {
					player.el.jPlayer('play');
				} else {
					player.el.jPlayer('playHead', 0);
					player.el.jPlayer('play');
				}
			}
		},

		pause: function(player) {
			if(player && player.loaded) {
				if(player.playing) {
					player.el.jPlayer('pause');	
				}
			}
		},

		setupWaypoint: function(player) {
			player.waypoint = new Waypoint.Inview({
				element: player.el[0],
				enter: function(direction) {
					if(!_ipad) {
						Video.play(player);	
					}
				},
				exited: function(direction) {
					Video.pause(player);
				}
			});
		},

		toggleMute: function(player) {
			if(!player.restarted) {
				Video.playFromStart(player);	
			}
			player.muteEl.find('.mute-option').each(function() {
				$(this).toggleClass('hide');
			});

			var vol = Video.isMuted(player) ? 0 : 0.8;
			player.el.jPlayer('volume', vol);
		},

		isMuted: function(player) {
			return player.muteEl.find('.audio-on').hasClass('hide');
		},

		unMute: function(player) {
			if (Video.isMuted(player)) {
				Video.toggleMute(player);
			}
		},

		anyVideoUnmuted: function() {
			var notMuted = $('.video-item--mute .mute-option.audio-on').not('.hide');
			return notMuted.length > 0;
		},

		loadMobile: function(player) {
			player.ipadEl.remove();
			player.el.jPlayer('setMedia', {
				poster: _imgPath + player.id + '.jpg',
				m4v: Video.path + player.id + '.mp4'
			});
			player.loaded = true;
			Video.play(player);
		},

		setupClickEvent: function(player) {
			player.el.addClass('pointer');
			player.el.on('click', function() {
				if(_mobile) {
					Video.loadMobile(player);
				} else {
					Video.toggle(player);
				}
			});

			// mute
			player.muteEl.on('click', function() {
				Video.toggleMute(player);
			});
		}
	};

	init();
})();