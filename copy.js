var archieml = require('archieml');
var request = require('request');
var fs = require('fs');

// GOOGLE DOC ID GOES HERE
var id = '1DEx9yImqhrp5T5h8vUStXSz9sq7EbkXEj44GGKsgxpY';
var url = 'https://docs.google.com/document/d/' + id + '/export?format=txt';

if(id) {
	request(url, function (error, response, body) {
		var parsed = archieml.load(body);

		parsed['story'].forEach(function(d) {
			// replace newline and return carriage with close/open tags
			// put first and last tags
			d.text = '<p>' + (d.text.replace(/(\r\n)+/g, '</p><p>')) + '</p>';
		});
		var str = JSON.stringify(parsed);
		var output = 'src/data/copy.json';

		fs.writeFile(output, str, function(err){
			if(err) { 
				console.log(err);
			} else {
				console.log('file created at', output);
			}
		});
	});
} else {
	console.log('*** Add your google doc ID to copy.js ***');
}
